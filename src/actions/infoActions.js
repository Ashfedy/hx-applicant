import * as types from './actionTypes';
import {ajaxGet, ajaxPost, json} from '../components/common/Ajax';
import $ from 'jquery';

export function loadFormSuccess(formInput) {
  return {type: types.LOAD_FORM_SUCCESS, formInput};
}

export function loadForm(acc_uuid, job_uuid) {
  return function(dispatch){
    return ajaxGet(`https://s3-ap-southeast-1.amazonaws.com/applicantinfo/`+acc_uuid+`/`+job_uuid+`.json`).then( response => {
      let data = json(response);
      dispatch(loadFormSuccess(data));
    }).catch( error => {
      throw(error);
    });
  };
}

export function addApplicantInfoSuccess(response) {
  return {type: types.ADD_APPLICANT_INFO_SUCCESS, response};
}

export function addApplicantInfo(formData){
  return function(dispatch){
    let formParsedData = $.param(formData);
    return ajaxPost(`http://pivtest1.space/react.php`, formParsedData).then( response => {
      dispatch(addApplicantInfoSuccess(response));
    }).catch( error => {
      throw(error);
    });
  };
}
