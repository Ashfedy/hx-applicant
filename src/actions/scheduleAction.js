import * as types from './actionTypes';
import {ajaxPost, json} from '../components/common/Ajax';

export function scheduleInterviewSuccess(user) {
  return {type: types.SCHEDULE_INTERVIEW_SUCCESS, user};
}

export function scheduleInterviewFailure() {
  return {type: types.SCHEDULE_INTERVIEW_FAILURE};
}

export function scheduleInterview(scheduleValue) {
  return function(dispatch){
    return ajaxPost(`http://pivtest1.space/test.php`, scheduleValue).then( response => {
      let data = json(response);
      if(data[0].applicantinfo[2].applicant_status.active_stage != '') {
        dispatch(scheduleInterviewSuccess(data));
      }
      else {
        dispatch(scheduleInterviewFailure());
      }
    }).catch( error => {
      throw(error);
    });
  };
}
