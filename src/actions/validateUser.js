import * as types from './actionTypes';
import {ajaxGet, json} from '../components/common/Ajax';

export function validateUserSuccess(applicantinfo) {
  // debugger;
    return {type: types.VALIDATE_USER_SUCCESS, applicantinfo, valid_user:true};
}

export function validateUserFailure() {
    return {type: types.VALIDATE_USER_FAILURE, valid_user:false};
}

export function validateUser(localKey, acc_uuid, app_uuid) {
  // debugger;
  return function(dispatch){
    return ajaxGet(`https://s3-ap-southeast-1.amazonaws.com/applicantinfo/`+acc_uuid+`/`+app_uuid+`.json`).then( response => {
      let data = json(response);
      let appKey = data[3].system.id;
      if(localKey == appKey){dispatch(validateUserSuccess(data));}
      else{dispatch(validateUserFailure());}
    }).catch( error => {
      throw(error);
    });
  };
}
