import * as types from './actionTypes';

export function nextStage(currentStage) {
  // debugger;
    currentStage++;
    return {type: types.NEXT_STAGE, current_stage:currentStage};
}

export function previousStage(currentStage) {
  // debugger;
    currentStage--;
    return {type: types.PREVIOUS_STAGE, current_stage:currentStage};
}
