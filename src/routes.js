import React from 'react';
import { Route, IndexRoute} from 'react-router';

import App from './components/App';
import Home from './components/home/Home';
import NotFound from './components/home/NotFound';
import ApplicantMain from './components/applicant/ApplicantMain';
import ApplicantInfo from './components/applicant/ApplicantInfo';
import ApplicantAssess from './components/applicant/ApplicantAssess';
import ApplicantStatus from './components/applicant/ApplicantStatus';
import About from './components/About';

export default (
  <Route path="/" component={App}>
    <IndexRoute component={Home}/>
    <Route path="/about" component={About}/>
    <Route path="/:id" component={ApplicantMain}/>
    <Route path="/:id/applicantinfo" component={ApplicantInfo}/>
    <Route path="/:id/applicantassess" component={ApplicantAssess}/>
    <Route path="/:id/Applicantstatus" component={ApplicantStatus}/>
    <Route path="/:id/*" component={NotFound}/>
  </Route>
);
