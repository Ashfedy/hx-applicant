/* eslint-disable import/default */

import 'babel-polyfill';
import React from 'react';
import {render} from 'react-dom';
import configureStore from './store/configureStore';
import {Provider} from 'react-redux';
import { Router, browserHistory } from 'react-router';
import routes from './routes';
// import {loadForm} from './actions/infoActions';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import '../node_modules/alertifyjs/build/css/alertify.min.css';
import '../node_modules/react-bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css';
import './styles/index.less';

const store = configureStore();
// debugger;
// debugger;

render(
  <Provider store={store}>
    <Router history={browserHistory} routes={routes} />
  </Provider>,
    document.getElementById('hx_app')
);
