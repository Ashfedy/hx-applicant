import React from 'react';

  const Header = (header) => {
    return (
      <header>
        <div id="companyLogo">
          <img src={(header.url != undefined) ? header.url:"https://s3-ap-southeast-1.amazonaws.com/chlogo2/hirexact_logo_big.png"}/>
        </div>
        {header.has_app_uuid && header.valid_user &&
        (
        <div id="headerApplicantDetails">
          <p>Job title: <span>{header.job_title}</span></p>
        </div>
        )
        }
      </header>
    );
};
// debugger;
// Header.propTypes = {
//   app_id: PropTypes.string.isRequired,
//   job_id: PropTypes.string.isRequired,
//   has_app_uuid: PropTypes.bool
// };

export default Header;
