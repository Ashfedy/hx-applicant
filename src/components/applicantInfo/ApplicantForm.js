import React, {PropTypes} from 'react';
import TextInput from '../common/inputs/TextInput';
import TextAreaInput from '../common/inputs/TextAreaInput';
import SelectInput from '../common/inputs/SelectInput';
import NumberInput from '../common/inputs/NumberInput';
import PasswordInput from '../common/inputs/PasswordInput';
import EmailInput from '../common/inputs/EmailInput';
import RadioInput from '../common/inputs/RadioInput';
import CheckboxInput from '../common/inputs/CheckboxInput';
import Submit from '../common/inputs/Submit';


const ApplicantForm = ({formInfo, app_uuid, job_uuid, acc_uuid}) => {

  return (
    <div className="applicant-info-container" id="applicantInfoContainer">
      <form className="form-horizontal" id="applicantInforForm">
          {
            formInfo.map((formInput,index) => {
              // debugger;
              switch (formInput.type) {
                case "text":
                  return (
                    <TextInput key={index} id={index} name={formInput.name} label={formInput.label} description={formInput.description} required={formInput.required}/>
                  );

                case "select":
                  return (
                    <SelectInput key={index} id={index} name={formInput.name} label={formInput.label} description={formInput.description} options={formInput.options} required={formInput.required}/>
                  );

                case "number":
                  return (
                    <NumberInput key={index} id={index} name={formInput.name} label={formInput.label} description={formInput.description} required={formInput.required}/>
                  );

                case "textarea":
                  return (
                    <TextAreaInput key={index} id={index} name={formInput.name} label={formInput.label} description={formInput.description} required={formInput.required}/>
                  );

                case "password":
                  return (
                    <PasswordInput key={index} id={index} name={formInput.name} label={formInput.label} description={formInput.description} required={formInput.required}/>
                  );

                case "email":
                  return (
                    <EmailInput key={index} id={index} name={formInput.name} label={formInput.label} description={formInput.description} required={formInput.required}/>
                  );

                case "radio":
                  return (
                    <RadioInput key={index} id={index} name={formInput.name} label={formInput.label} description={formInput.description} options={formInput.options} required={formInput.required}/>
                  );

                case "checkbox":
                  return (
                    <CheckboxInput key={index} id={index} name={formInput.name} label={formInput.label} description={formInput.description} options={formInput.options} required={formInput.required}/>
                  );

                default:
                  break;
              }
            }
          )}
          <input type="hidden" name="app_id" value={app_uuid}/>
          <input type="hidden" name="job_id" value={job_uuid}/>
          <input type="hidden" name="acc_id" value={acc_uuid}/>
          <Submit/>
          <p className="mandatory-info display-none">All * fields are mandatory</p>
      </form>
    </div>
  );
};

ApplicantForm.propTypes = {
  formInfo: PropTypes.array.isRequired,
  app_uuid: PropTypes.string.isRequired,
  job_uuid: PropTypes.string.isRequired,
  acc_uuid: PropTypes.string.isRequired
};

export default ApplicantForm;
