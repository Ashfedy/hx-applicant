import React,{PropTypes} from 'react';

const ApplicantDetails = ({applicant_details}) => {
  // return(
  //   <div className="applicant-info-container" id="applicantInfoContainer">
  //     {
  //       applicant_details.map((applicant, index) => {
  //         return(
  //           <div key={index} className="applicant-details">
  //             <label className="applicant-detail-label">{applicant.label}</label>
  //             <span>:</span>
  //             <label className="applicant-detail-value">{applicant.value}</label>
  //           </div>
  //         );
  //       })
  //     }
  //   </div>
  // );
  return (
    <h3 className="text-center alert alert-success fade in">{applicant_details[0].message}</h3>
  );
};

ApplicantDetails.propTypes = {
  applicant_details: PropTypes.array.isRequired
};

export default ApplicantDetails;
