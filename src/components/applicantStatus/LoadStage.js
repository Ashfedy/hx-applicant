import React, {PropTypes} from 'react';
import StatusHeader from './StatusHeader';
import StageDetails from './StageDetails';
import MainContent from './MainContent';

const LoadStage = ({activeStage, currentStage, status, job}) => {
  // debugger;
  return(
    <div className="applicant-status-container">
      <StatusHeader activeStage={activeStage} currentStage={currentStage} title={status.stages[currentStage-1].title} status={status.status} length={status.stages.length}/>
      <StageDetails stage={status.stages[currentStage-1]}/>
      <MainContent job={job} activeStage={activeStage} currentStage={currentStage} stage={status.stages[currentStage-1]} cancel={status.cancel_interview} cancelledDetails={status.cancel_details} reschedule={status.reschedule_request} rescheduleDetails={status.reschedule_details}/>
    </div>
  );
};

LoadStage.propTypes = {
  status: PropTypes.object.isRequired,
  job: PropTypes.object.isRequired,
  activeStage: PropTypes.number.isRequired,
  currentStage: PropTypes.number.isRequired
};

export default LoadStage;
