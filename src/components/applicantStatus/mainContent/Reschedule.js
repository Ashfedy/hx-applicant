import React, {PropTypes} from 'react';

const Reschedule = ({details}) => {
  return (
    <div className="interview-schedule-container">
      <div className="interview-reschedule-conducted-section" id="interviewRescheduleSection">
        <div className="interview-main-details">
          <label>Reschedule requested</label>
          <span>:</span>
          <p>{details.date}, {details.time}</p>
        </div>
        <div className="interview-main-details">
          <label>Result</label>
          <span>:</span>
          <p>N/A</p>
        </div>
      </div>
    </div>
  );
};

Reschedule.propTypes = {
  details: PropTypes.object.isRequired
};

export default Reschedule;
