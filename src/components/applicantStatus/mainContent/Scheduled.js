import React, {PropTypes} from 'react';

const Scheduled = ({details, job}) => {
  return (
    <div className="interview-schedule-container">
      <div className="interview-scheduled-section">
        <p>Your {details.type} interview is scheduled on:</p>
        <h3 className="scheduled-details">{details.interview_schedule_accepted.date}, {details.interview_schedule_accepted.time}</h3>
        <h5>You can contact {job.owner} at {job.owner_email} or {job.owner_number} for any clarifications.</h5>
      </div>
    </div>
  );
};

Scheduled.propTypes = {
  details: PropTypes.object.isRequired,
  job: PropTypes.object.isRequired
};

export default Scheduled;
