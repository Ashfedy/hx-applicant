import React from 'react';

const NotScheduled = () => {
  return (
    <h2 id="interviewNotScheduledText" className="text-center">Interview not scheduled yet</h2>
  );
};

export default NotScheduled;
