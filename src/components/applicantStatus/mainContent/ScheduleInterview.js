import React, {PropTypes} from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import * as schedule from  '../../../actions/scheduleAction';
import ScheduleOptions from './schedule/ScheduleOptions';
import PreferredSchedule from './schedule/PreferredSchedule';
import $ from 'jquery';

class ScheduleInterview extends React.Component{
  constructor(props) {
    super(props);
  }

  scheduleSubmit() {
    let preferredvalue = $('[name="preferredDate"]').val();
    if(preferredvalue != '') {
      let preferredDate = $('[name="preferredDate"]').val();
      let preferredTime = $('[name="preferredTime"]').val();
      let value = {
        "preferred_date": preferredDate,
        "preferred_time": preferredTime
      };
        this.props.actions.scheduleInterview(value);
    }
    else {
      let checked = $('[name="slot_confirm"]:checked').length;
      if(checked!=0) {
        let date = $('[name="slot_confirm"]:checked').attr('data-date');
        let time = $('[name="slot_confirm"]:checked').attr('data-time');
        let value = {
          "date": date,
          "time": time
        };
        this.props.actions.scheduleInterview(value);
      }
    }
  }

  render() {
    let expired = 0;
    let allExpired = false;
    this.props.details.interview_schedule_options.map((slot) => {
      const currentTime = new Date().getTime() / 1000;
      let scheduleDateTime = slot.seconds;
      if(currentTime > scheduleDateTime){
        expired = expired + 1;
      }
    });

    if(expired == 3){
      allExpired = true;
    }

    return (
      <div className="schedule-options-container">
        <ScheduleOptions details={this.props.details} />
        <PreferredSchedule allExpired={allExpired}/>
        <input type="submit" name="" className="btn btn-success hx-submit" onClick={this.scheduleSubmit.bind(this)}/>
      </div>
    );
  }
}

ScheduleInterview.propTypes = {
  details: PropTypes.object.isRequired,
  actions: PropTypes.object.isRequired
};

function mapStateToProps() {
  return {};
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(schedule, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ScheduleInterview);
