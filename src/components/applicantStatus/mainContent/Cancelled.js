import React, {PropTypes} from 'react';

const Cancelled = ({details, job}) => {
  return (
    <div className="interview-schedule-container">
      <div className="interview-cancel-section">
        <p>This interview was cancelled by {job.owner} because of the following reason:</p>
        <h3 className="cancelled-reason">{details}</h3>
        <h5>You can contact {job.owner} at {job.owner_email} or {job.owner_number} for any clarifications.</h5>
      </div>
    </div>
  );
};

Cancelled.propTypes = {
  details: PropTypes.string.isRequired,
  job: PropTypes.string.isRequired
};

export default Cancelled;
