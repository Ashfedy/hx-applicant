import React, {PropTypes} from 'react';

const Conducted = ({details}) => {
  return (
    <div className="interview-schedule-container">
      <div className="interview-reschedule-conducted-section" id="interviewConductedSection">
        <div className="interview-main-details">
          <label>Conducted on</label>
          <span>:</span>
          <p>{details.conducted_on}</p>
        </div>
        <div className="interview-main-details">
          <label>Result</label>
          <span>:</span>
          <p>{details.result}</p>
        </div>
      </div>
    </div>
  );
};

Conducted.propTypes = {
  details: PropTypes.object.isRequired
};

export default Conducted;
