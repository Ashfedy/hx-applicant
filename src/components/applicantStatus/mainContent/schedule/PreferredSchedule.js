import React, {PropTypes} from "react";
import DateTimeField from "react-bootstrap-datetimepicker";
import moment from "moment";

class PreferredSchedule extends React.Component{

  constructor(props) {
    super(props);
  }

  render() {
    let dateTimePickerAttr = {
      "name":"preferredDate",
      "className":"hx-textbox"
    };
    return (
      <div className="preferred-schedule-section">
        {!this.props.allExpired &&
          <div>
            <div className="interview-section-or-divider">
              <hr/>
              <p>OR</p>
            </div>
            <p>If none of three schedules suit you, please indicate a preferred date and time. We will try to accommodate your preference and reschedule an interview accordingly.</p>
        </div>
      }
      {this.props.allExpired &&
        <div>All Slots are expired</div>
      }
      <div className="preferred-date-time">
          <h4>Preferred date & time</h4>
          <div className="row date-time-picker-wrapper">
            <div className="col-xs-11 date-time-picker-container">
              <DateTimeField defaultText="Please select a date" mode="date" inputFormat="DD/MM/YYYY" maxDate={moment().add(365, "days")} minDate={moment().subtract(0, "days")} inputProps={dateTimePickerAttr}/>
            </div>
            <div className="col-xs-11 date-time-picker-container pull-right">
              <select name="preferredTime" className="hx-select">
                <option value="1">Anytime</option>
                <option value="2">Morning(9-12)</option>
                <option value="3">Afternoon(1-4)</option>
                <option value="4">Evening(5-8)</option>
              </select>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

PreferredSchedule.propTypes = {
  allExpired: PropTypes.bool.isRequired
};

export default PreferredSchedule;
