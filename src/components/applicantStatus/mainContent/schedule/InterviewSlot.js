import React, {PropTypes} from 'react';
import $ from 'jquery';

const InterviewSlot = ({slot, expired, index}) => {
  const radioChecked = () => {
    $('.interview-details-reset').removeClass('display-none');
    $('.preferred-schedule-section').hide();
    $('[name="preferredDate"]').val('');
  };

  return (
    <div className="interview-slot">
        <div className="row">
          <div className="col-lg-4 col-md-4 col-sm-3 col-xs-3">
            <p>Slot {index+1}</p>
          </div>
          <div className="col-lg-4 col-md-5 col-sm-6 col-xs-6">
            <p className="slot-date-time">{slot.date}, {slot.time}</p>
          </div>
          <div className="col-lg-1 col-md-2 col-sm-3 col-xs-3">
            {!expired && <input type="radio" value={index+1} name="slot_confirm" className="" data-date={slot.date} data-time={slot.time} onClick={radioChecked}/>}
            {expired && <div className="glyphicon glyphicon-exclamation-sign expired" title="expired"></div>}
          </div>
        </div>
    </div>
  );
};

InterviewSlot.propTypes = {
  slot: PropTypes.object.isRequired,
  expired: PropTypes.bool.isRequired,
  index: PropTypes.number
};

export default InterviewSlot;
