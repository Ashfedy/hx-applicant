import React, {PropTypes} from 'react';
import InterviewSlot from './InterviewSlot';
import $ from 'jquery';

const ScheduleOptions = ({details}) => {
  const reset = () => {
    $('[name="slot_confirm"]').removeAttr('checked');
    $('.preferred-schedule-section').show();
    $('[name="preferredDate"]').val('');
    $('[name="preferredTime"]').val(1);
    $('.interview-details-reset').addClass('display-none');
  };
  return (
    <div className="schedule-options-section">
      <p>Please select a time slot from the following options for your interview.</p>
      <div className="interview-schedule-details">
        {
        details.interview_schedule_options.map((slot,index) => {
          const currentTime = new Date().getTime() / 1000;
          let scheduleDateTime = slot.seconds;
          let expired;
          if(currentTime < scheduleDateTime) expired = false;
          else expired = true;
          // let scheduleDateTime = [true, false, true];

          return (
            <InterviewSlot key={index} slot={slot} index={index} expired={expired}/>
          );
        })
      }
        <div className="interview-details-reset display-none" onClick={reset}>
          <p className="text-center">Reset</p>
        </div>
      </div>
    </div>
  );
};

ScheduleOptions.propTypes = {
  details: PropTypes.object.isRequired
};

export default ScheduleOptions;
