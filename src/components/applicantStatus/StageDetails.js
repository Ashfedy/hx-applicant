import React, {PropTypes} from 'react';

const StageDetails = ({stage}) => {
  return(
    <div className="applicant-status-details-container">
      <div className="applicant-status-sub-details" id="stageInformation">
        <label>Information</label>
        <span>:</span>
        <p>{stage.information != "" ? stage.information:"N/A"}</p>
      </div>
      <div className="applicant-status-sub-details" id="stageFeedback">
        <label>Feedback</label>
        <span>:</span>
        <p>{stage.feedback != "" ? stage.feedback:"N/A"}</p>
      </div>
      <div className="applicant-status-sub-details" id="stageMode">
        <label>Mode</label>
        <span>:</span>
        <p>{stage.type != "" ? stage.type:"N/A"}</p>
      </div>
      <div className="applicant-status-sub-details" id="StageDescription">
        <label>Details</label>
        <span>:</span>
        <p>{stage.description != "" ? stage.description:"N/A"}</p>
      </div>
    </div>
  );
};

StageDetails.propTypes = {
  stage: PropTypes.object.isRequired
};


export default StageDetails;
