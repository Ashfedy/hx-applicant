import React, {PropTypes} from 'react';
import Reschedule from './mainContent/Reschedule';
import NotScheduled from './mainContent/NotScheduled';
import ScheduleInterview from './mainContent/ScheduleInterview';
import Scheduled from './mainContent/Scheduled';
import Conducted from './mainContent/Conducted';
import Cancelled from './mainContent/Cancelled';

const MainContent = ({job, stage, cancel, cancelledDetails, reschedule, rescheduleDetails, currentStage, activeStage}) => {
  return (
    <div className="applicant-status-main-container">
      {!cancel && reschedule && currentStage==activeStage && <Reschedule details={rescheduleDetails}/>}
      {cancel && currentStage==activeStage && <Cancelled details={cancelledDetails} job={job}/>}
      {stage.interview_schedule_options.length == 0 && <NotScheduled />}
      {stage.interview_schedule_options.length != 0 && stage.interview_schedule_accepted.date=="" && <ScheduleInterview details={stage}/>}
      {!cancel && !reschedule && stage.interview_schedule_options.length != 0 && stage.interview_schedule_accepted.date!="" && stage.conducted_on=="" && <Scheduled details={stage} job={job}/>}
      {stage.interview_schedule_options.length != 0 && stage.interview_schedule_accepted.date!="" && stage.conducted_on!="" && <Conducted details={stage}/>}
    </div>
  );
};

MainContent.propTypes = {
  stage: PropTypes.object.isRequired,
  job: PropTypes.object,
  reschedule: PropTypes.bool.isRequired,
  cancel: PropTypes.bool.isRequired,
  cancelledDetails: PropTypes.string.isRequired,
  rescheduleDetails: PropTypes.object.isRequired,
  activeStage: PropTypes.number.isRequired,
  currentStage: PropTypes.number.isRequired
};

export default MainContent;
