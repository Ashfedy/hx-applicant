import React, {PropTypes} from 'react';

const StatusHeader = ({activeStage, currentStage, title, status, length}) => {

  return(
    <div className="applicant-status-header-container">
      {activeStage==currentStage && status == "onhold" && <p className="applicant-stage-number on-hold">{currentStage+" of "+length}</p>}
      {activeStage==currentStage && status != "onhold" && <p className="applicant-stage-number active">{currentStage+" of "+length}</p>}
      {activeStage!=currentStage && <p className="applicant-stage-number">{currentStage+" of "+length}</p>}
      <p className="applicant-stage-title">{title}</p>
      {activeStage==currentStage && <h2>You are currently on Stage {currentStage} of the selection process</h2>}
      {activeStage!=currentStage && <h2>Stage {currentStage}</h2>}
    </div>
  );
};

StatusHeader.propTypes = {
  title: PropTypes.string.isRequired,
  status: PropTypes.string.isRequired,
  activeStage: PropTypes.number.isRequired,
  currentStage: PropTypes.number.isRequired,
  length: PropTypes.number.isRequired
};


export default StatusHeader;
