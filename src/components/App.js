//This component handles the App template used on every page
import React, {PropTypes} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import Header from './common/Header';
import * as validateUser from  '../actions/validateUser';
import * as job from '../actions/infoActions';
import Home from './home/Home';
import NotFound from './home/NotFound';
// import * as alertify from 'alertifyjs';

class App extends React.Component{
  constructor(props, context){
    super(props, context);
    this.state = {
      userAppCode: ''
    };
  }

  updateInput(event) {
    let userAppCode = this.state.userAppCode;
    userAppCode = event.target.value;
    return this.setState({userAppCode:userAppCode});
  }

  enter(event) {
    if(event.keyCode == 13) {
      this.validateAppCode();
    }
  }

  validateAppCode() {
    this.props.actions.validateUser(this.state.userAppCode, this.props.acc_uuid,this.props.app_uuid);
  }

  render() {
    // debugger;
    if(this.props.applicantinfo.valid_user == true && this.props.has_app_uuid == true && (this.props.jobinfo.form_details.length == 0)){
      this.props.actions.loadForm(this.props.acc_uuid,this.props.job_uuid);
    }
    return (
      <div className="hx-wrapper">

        {(this.props.applicantinfo.valid_user == true && this.props.has_app_uuid == true) &&
          <div className="container">
            {<Header url={this.props.jobinfo.job_details.company_logo} app_id={this.props.applicantinfo.applicantinfo[0].applicant_info.id} job_title={this.props.jobinfo.job_details.job_title} has_app_uuid={this.props.has_app_uuid} valid_user={this.props.applicantinfo.valid_user}/>}
            {!(this.props.applicantinfo.valid_user) && !(this.props.has_app_uuid) && this.props.has_valid_url && <Home />}
            {this.props.applicantinfo.valid_user && this.props.has_valid_url && this.props.children}
          </div>
        }

          {(this.props.applicantinfo.valid_user == false && this.props.has_app_uuid == true) &&
            <div id="userValidation">
              <h3>Please Enter Your Applicant Code</h3>
              <input type="password" name="userAppCode" className="hx-textbox" id="userAppCode" value={this.state.userAppCode} onChange={this.updateInput.bind(this)} onKeyDown={this.enter.bind(this)} autoFocus/>
              <button onClick={this.validateAppCode.bind(this)}>Enter</button>
            </div>
          }

            {!(this.props.applicantinfo.valid_user) && !(this.props.has_app_uuid) && this.props.has_valid_url &&
              <div className="container">
                <Header url={this.props.jobinfo.job_details.company_logo}/>
                {this.props.children}
              </div>
            }

              {!(this.props.has_valid_url) &&
                <div className="container">
                  <Header url="https://s3-ap-southeast-1.amazonaws.com/chlogo2/hirexact_logo_big.png"/>
                  <NotFound/>
                </div>
              }

            </div>
            // <h1>Hello</h1>
          );
        }
      }

      const isValidURL = (url) => {
        let pattern = new RegExp(/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{60,}$/i);
        return pattern.test(url);
      };

      App.propTypes = {
        children: PropTypes.object.isRequired,
        acc_uuid: PropTypes.string.isRequired,
        app_uuid: PropTypes.string.isRequired,
        job_uuid: PropTypes.string.isRequired,
        has_app_uuid: PropTypes.bool.isRequired,
        has_valid_url: PropTypes.bool.isRequired,
        URLParams: PropTypes.object,
        applicantinfo: PropTypes.object.isRequired,
        jobinfo: PropTypes.object,
        actions: PropTypes.object.isRequired
      };

      function mapStateToProps(state, ownProps) {
        const URLParams = ownProps.params;
        const acc_job_app_id = ownProps.params.id;
        let app_uuid='';
        let job_uuid='';
        let acc_uuid='';
        let has_app_uuid=false;
        let has_valid_url = isValidURL(acc_job_app_id);
        if(acc_job_app_id!=undefined){
          if(has_valid_url){
            app_uuid = acc_job_app_id.substring(40, 60);
            job_uuid = acc_job_app_id.substring(20, 40);
            acc_uuid = acc_job_app_id.substring(0, 20);
            has_app_uuid = true;
          }
          else{
            has_app_uuid = false;
          }
        }
        else{
          has_app_uuid = false;
          has_valid_url=true;
        }
        // debugger;
        return {
          applicantinfo: state.user[0],
          jobinfo: state.applicantInfo[0],
          acc_uuid: acc_uuid,
          app_uuid: app_uuid,
          job_uuid: job_uuid,
          has_app_uuid: has_app_uuid,
          has_valid_url: has_valid_url,
          URLParams: URLParams
        };
      }

      function mapDispatchToProps(dispatch) {
        return{
          actions: bindActionCreators(Object.assign({},validateUser,job), dispatch)
        };
      }

      export default connect(mapStateToProps, mapDispatchToProps)(App);
