import React, {PropTypes} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import LoadStage from '../applicantStatus/LoadStage';
import * as navigateStage from '../../actions/navigateStage';
import * as schedule from '../../actions/scheduleAction';
// import * as infoActions from '../../actions/infoActions';
// import $ from 'jquery';

class ApplicantStatus extends React.Component{
  constructor(props, context) {
    super(props, context);
    this.state = {};
  }

  nextStage() {
    let currentStage = this.props.currentStage != 0 ? this.props.currentStage:this.props.applicantStatus.active_stage;
    if(currentStage != this.props.applicantStatus.stages.length){
      this.props.actions.nextStage(currentStage);
    }
  }

  previousStage() {
    let currentStage = this.props.currentStage != 0 ? this.props.currentStage:this.props.applicantStatus.active_stage;
    if(currentStage > 1){
        this.props.actions.previousStage(currentStage);
    }
  }

  render() {
    return (
      <div className="applicant-content-container">
        <div className="status-navigation-container">
          <input type="button" value="previous" id="statusPrevious" onClick={this.previousStage.bind(this)}/>
          <input type="button" value="next" id="statusNext" onClick={this.nextStage.bind(this)}/>
        </div>
        <div className="hx-content-title-container">
          <a className="hx-content-navigator left pull-left" href="applicantassess"><span className="glyphicon glyphicon-chevron-left"></span></a>
          <h2 className="hx-content-title">Applicant Status</h2>
        </div>
        <LoadStage status={this.props.applicantStatus} job={this.props.job} currentStage={this.props.currentStage != 0 ? this.props.currentStage:this.props.applicantStatus.active_stage} activeStage={this.props.applicantStatus.active_stage}/>
      </div>
    );
  }
}

ApplicantStatus.propTypes = {
  aja: PropTypes.string.isRequired,
  applicantStatus: PropTypes.object,
  job: PropTypes.object,
  currentStage: PropTypes.number.isRequired,
  actions: PropTypes.object.isRequired
};

function mapStateToProps(state, ownProps) {
  const acc_job_app_id = ownProps.params.id;
  return {
    aja: acc_job_app_id,
    applicantStatus: state.user[state.user.length-1].applicantinfo[2].applicant_status,
    job: state.user[state.user.length-1].applicantinfo[3].system.job[0],
    currentStage: state.status[0].current_stage
  };
}

function mapDispatchToProps(dispatch) {
  // debugger;
  return {
    actions: bindActionCreators(Object.assign(navigateStage, schedule), dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ApplicantStatus);
