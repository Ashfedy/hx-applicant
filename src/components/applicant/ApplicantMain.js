import React, {PropTypes} from 'react';
import {connect} from 'react-redux';

class ApplicantMain extends React.Component{
  constructor(props, context) {
    super(props, context);
  }
  render() {
    return (
      <div className="applicant-home-container">
        <h2 className="greetings">Hi Applicant {this.props.greetings}</h2>
        <h1>{this.props.defaultMessage}</h1>
        {(Object.keys(this.props.applicantinfo.applicantinfo[2].applicant_status).length === 0 && this.props.applicantinfo.applicantinfo[2].applicant_status.constructor === Object) && <h3 className="default-message">{this.props.applicantinfo.applicantinfo[3].system.default_message}</h3>}
      </div>
    );
  }
}

ApplicantMain.propTypes = {
  applicantinfo: PropTypes.object.isRequired,
  defaultMessage: PropTypes.string.isRequired,
  aja: PropTypes.string.isRequired,
  greetings: PropTypes.string.isRequired
};

function mapStateToProps(state, ownProps) {
  let acc_job_app_id = ownProps.params.id;
  const time = new Date().getHours();
  let greetings = '';
  if(time >= 16) greetings = 'Good Evening!';
  else if(time >= 12) greetings = 'Good Afternoon!';
  else greetings = 'Good Morning!';
  // debugger;
  return {
    applicantinfo: state.user[0],
    defaultMessage: state.user[0].applicantinfo[3].system.default_message,
    aja: acc_job_app_id,
    greetings: greetings
  };
}

export default connect(mapStateToProps)(ApplicantMain);
