import React, {PropTypes} from 'react';
import {connect} from 'react-redux';
// import $ from 'jquery';

class ApplicantAssess extends React.Component{
  constructor(props, context) {
    super(props, context);
    this.state = {};
  }

  render() {
    return (
      <div className="applicant-content-container">
        <div className="hx-content-title-container">
          <a className="hx-content-navigator left pull-left" href="applicantinfo"><span className="glyphicon glyphicon-chevron-left"></span></a>
          <h2 className="hx-content-title">Applicant Assessment</h2>
          <a className="hx-content-navigator right pull-right" href="applicantstatus"><span className="glyphicon glyphicon-chevron-right"></span></a>
        </div>
      </div>
    );
  }
}
// debugger;
ApplicantAssess.propTypes = {
  aja: PropTypes.string.isRequired
};

function mapStateToProps(state, ownProps) {
  const acc_job_app_id = ownProps.params.id; //from the path applicantinfo/:id

  return {
    aja: acc_job_app_id
  };
}

export default connect(mapStateToProps)(ApplicantAssess);
