import React, {PropTypes} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as infoActions from '../../actions/infoActions';
import ApplicantForm from '../applicantInfo/ApplicantForm';
import ApplicantDetails from '../applicantInfo/ApplicantDetails';

class ApplicantInfo extends React.Component{
  constructor(props, context) {
    super(props, context);
  }

  render() {
    const {applicantInfo} = this.props;

      return (
        <div className="applicant-content-container">
          <div className="hx-content-title-container">
            <a className="hx-content-navigator left pull-left" href={'/'+this.props.aja}><span className="glyphicon glyphicon-chevron-left"></span></a>
            <h2 className="hx-content-title">Applicant Information</h2>
            <a className="hx-content-navigator right pull-right" href="applicantassess"><span className="glyphicon glyphicon-chevron-right"></span></a>
          </div>
          {this.props.userDetails[0].applicant_info.length == 0 && <ApplicantForm formInfo={applicantInfo} app_uuid={this.props.app_uuid} job_uuid={this.props.job_uuid} acc_uuid={this.props.acc_uuid} />}
          {!(this.props.userDetails[0].applicant_info.length == 0) && <ApplicantDetails applicant_details={this.props.userDetails[0].applicant_info} />}
        </div>
      );
    }
  }
ApplicantInfo.propTypes = {
  applicantInfo: PropTypes.array.isRequired,
  userDetails: PropTypes.array.isRequired,
  aja: PropTypes.string.isRequired,
  app_uuid: PropTypes.string.isRequired,
  acc_uuid: PropTypes.string.isRequired,
  job_uuid: PropTypes.string.isRequired,
  actions: PropTypes.object.isRequired
};

function mapStateToProps(state, ownProps) {
  const acc_job_app_id = ownProps.params.id; //from the path applicantinfo/:id
  let app_uuid = acc_job_app_id.substring(40, 60);
  let job_uuid = acc_job_app_id.substring(20, 40);
  let acc_uuid = acc_job_app_id.substring(0, 20);
  return {
    applicantInfo: state.applicantInfo[0].form_details,
    userDetails: state.user[0].applicantinfo,
    aja: acc_job_app_id,
    app_uuid: app_uuid,
    job_uuid: job_uuid,
    acc_uuid: acc_uuid
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(infoActions, dispatch)
  };
}
export default connect(mapStateToProps, mapDispatchToProps)(ApplicantInfo);
