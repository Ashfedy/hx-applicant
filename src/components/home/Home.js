import React from 'react';
import {Link} from 'react-router';

const Home = () => {
    return (
      <div>
        <h1>Hirexact</h1>
        <p>About Hirexact and What this page is about</p>
        <Link to="/about">About</Link>
      </div>
    );
};
export default Home;
