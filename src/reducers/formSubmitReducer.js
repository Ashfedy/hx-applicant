import * as types from '../actions/actionTypes';
import initialState from './initialState';

export default function formSubmitReducer(state=initialState.formSubmit, action) {
  switch (action.type) {
    case types.ADD_APPLICANT_INFO_SUCCESS:
      return [ ...state, Object.assign({}, action)];

    default:
      return state;
  }
}
