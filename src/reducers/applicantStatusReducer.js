import * as types from '../actions/actionTypes';
import initialState from './initialState';

export default function applicantStatusReducer(state=initialState.status, action) {
  switch (action.type) {
    case types.NEXT_STAGE:
      return [Object.assign({}, state, action)];

    case types.PREVIOUS_STAGE:
      return [Object.assign({}, state, action)];

    default:
      return state;
  }
}
