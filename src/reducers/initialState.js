export default {
  applicantInfo: [
    {
      job_details:{

      },
      form_details:[]
    }
  ],
  user: [
    {
      valid_user: false,
      applicantinfo: [
        {
          applicant_info: []
        },
        {
          applicant_assess: []
        },
        {
          applicant_status: {}
        },
        {
          system: {}
        }
      ]
    }
  ],
  status: [
    {
      current_stage: 0
    }
  ],
  formSubmit: []
};
