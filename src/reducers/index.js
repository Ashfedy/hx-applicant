import {combineReducers} from "redux";
import applicantInfo from "./formReducer";
import formSubmit from "./formSubmitReducer";
import user from "./validateUserReducer";
import status from "./applicantStatusReducer";
// import schedule from "./scheduleReducer";

const rootReducer = combineReducers({
  applicantInfo,
  formSubmit,
  user,
  status
  // schedule
});

export default rootReducer;
