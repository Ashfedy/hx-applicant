import * as types from '../actions/actionTypes';
import initialState from './initialState';

export default function formReducer(state=initialState.applicantInfo, action) {
  switch (action.type) {
    case types.LOAD_FORM_SUCCESS:
    return action.formInput;

    default:
      return state;
  }
}
