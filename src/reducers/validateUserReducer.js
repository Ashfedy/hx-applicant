import * as types from '../actions/actionTypes';
import initialState from './initialState';

export default function validateUser(state=initialState.user, action) {
  switch(action.type) {

    case types.VALIDATE_USER_SUCCESS:
    {
      return [Object.assign({}, state, action)];
    }

    case types.VALIDATE_USER_FAILURE:
    {
      return [Object.assign({}, state, action)];
    }

    case types.SCHEDULE_INTERVIEW_SUCCESS:
    {
      return [ ...state, Object.assign({}, action.user[0])];
    }

    case types.SCHEDULE_INTERVIEW_FAILURE:
    {
      return state;
    }

    default:
      return state;
  }
}
